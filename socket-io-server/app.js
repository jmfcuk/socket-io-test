const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const port = process.env.PORT || 4001;
const index = require("./routes/index");

const app = express();

app.use(index);

const server = http.createServer(app);
const io = socketIo(server);

const onInterval = async socket => {

    socket.emit("update", new Date().toJSON());
};

let interval;
io.on("connection", socket => {
    console.log("client connected");
    if (interval) {
        clearInterval(interval);
    }
    interval = setInterval(() => onInterval(socket), 5000);
    socket.on("disconnect", () => {
        console.log("client disconnected");
    });
});

server.listen(port, () => console.log(`Listening on port ${port}`));
